package com.ho.smarthome;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;


public class ControlActivity extends AppCompatActivity {
    private String TAG = "Control";


    private ProgressDialog mProgressDialog;
    private SeekBar mBrightness;
    private SeekBar mCT;
    private SeekBar mColor;
    private Button mBtnOn;
    private Button mBtnOff;
    private Button mBtnMusic;

    YeelightCaller yeelightCaller;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case YeelightCaller.MSG_CONNECT_FAILURE:
                    mProgressDialog.dismiss();
                    break;
                case YeelightCaller.MSG_CONNECT_SUCCESS:
                    mProgressDialog.dismiss();
                    break;
            }
        }
    };

    private static int MIN_BRIGHTNESS =1;
    private static int MAX_BRIGHTNESS =100;

    private static int MIN_COLOR =0;
    private static int MAX_COLOR =350;

    private static int MIN_CT =1700;
    private static int MAX_CT =6500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        String mBulbIP = getIntent().getStringExtra("ip");
        int mBulbPort = Integer.parseInt(getIntent().getStringExtra("port"));
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Connecting...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        mBrightness = (SeekBar) findViewById(R.id.brightness);
        mColor = (SeekBar) findViewById(R.id.color);
        mCT = (SeekBar) findViewById(R.id.ct);
        mCT.setMax(MAX_CT-MIN_CT);
        mColor.setMax(MAX_COLOR-MIN_COLOR);

        mBrightness.setMax(MAX_BRIGHTNESS-MIN_BRIGHTNESS);


        yeelightCaller = new YeelightCaller(mBulbIP, mBulbPort);

        mBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d(TAG,"brightness:"+seekBar.getProgress()+MIN_BRIGHTNESS);
                yeelightCaller.setBrightness(seekBar.getProgress()+MIN_BRIGHTNESS);
//
            }
        });
        mCT.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d(TAG,"ct:"+seekBar.getProgress()+MIN_CT);
                yeelightCaller.setCT(seekBar.getProgress()+MIN_CT);

            }
        });
        mColor.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d(TAG,"color:"+seekBar.getProgress()+MIN_COLOR);
                yeelightCaller.setColor(seekBar.getProgress()+MIN_COLOR);

            }
        });
        mBtnOn = (Button) findViewById(R.id.btn_on);
        mBtnOff = (Button) findViewById(R.id.btn_off);
        mBtnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yeelightCaller.on();

            }
        });
        mBtnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yeelightCaller.off();

            }
        });
        yeelightCaller.connect(mHandler);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        yeelightCaller.close();


    }


}
