package com.ho.smarthome;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;


public class YeelightCaller {
    private static final String TAG = "Yeelight";

    public static final int MSG_CONNECT_SUCCESS = 0;
    public static final int MSG_CONNECT_FAILURE = 1;

    private static final String CMD_TOGGLE = "{\"id\":%id,\"method\":\"toggle\",\"params\":[]}\r\n";
    private static final String CMD_ON = "{\"id\":%id,\"method\":\"set_power\",\"params\":[\"on\",\"smooth\",500]}\r\n";
    private static final String CMD_OFF = "{\"id\":%id,\"method\":\"set_power\",\"params\":[\"off\",\"smooth\",500]}\r\n";
    private static final String CMD_CT = "{\"id\":%id,\"method\":\"set_ct_abx\",\"params\":[%value, \"smooth\", 500]}\r\n";
    private static final String CMD_HSV = "{\"id\":%id,\"method\":\"set_hsv\",\"params\":[%value, 100, \"smooth\", 200]}\r\n";
    private static final String CMD_BRIGHTNESS = "{\"id\":%id,\"method\":\"set_bright\",\"params\":[%value, \"smooth\", 200]}\r\n";
    private static final String CMD_BRIGHTNESS_SCENE = "{\"id\":%id,\"method\":\"set_bright\",\"params\":[%value, \"smooth\", 500]}\r\n";
    private static final String CMD_COLOR_SCENE = "{\"id\":%id,\"method\":\"set_scene\",\"params\":[\"cf\",1,0,\"100,1,%color,1\"]}\r\n";


    private int mCmdId;
    private Socket mSocket;
    private String mBulbIP;
    private int mBulbPort;
    private BufferedOutputStream mBos;
    private BufferedReader mReader;

    private boolean cmd_run = true;
    private int parseBrightness;

    public YeelightCaller(String mBulbIP, int mBulbPort) {
        this.mBulbIP = mBulbIP;
        this.mBulbPort = mBulbPort;
    }


    /**
     * @param mHandler need to close when not use
     */
    public void connect(final Handler mHandler) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cmd_run = true;
                    mSocket = new Socket(mBulbIP, mBulbPort);
                    mSocket.setKeepAlive(true);
                    mBos = new BufferedOutputStream(mSocket.getOutputStream());
                    mHandler.sendEmptyMessage(MSG_CONNECT_SUCCESS);
                    mReader = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
                    while (cmd_run) {
                        try {
                            String value = mReader.readLine();
//                            Log.d(TAG, "value = " + value);
                        } catch (Exception e) {

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.sendEmptyMessage(MSG_CONNECT_FAILURE);
                }
            }
        }).start();
    }


    public void connect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    Log.d(TAG, "connect");
                    cmd_run = true;
                    mSocket = new Socket(mBulbIP, mBulbPort);
                    mSocket.setKeepAlive(true);
                    mBos = new BufferedOutputStream(mSocket.getOutputStream());
                    mReader = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

                    while (cmd_run) {
                        try {
                            String value = mReader.readLine();
                            Log.d(TAG, "value = " + value);
                        } catch (Exception e) {

                        }

                    }

                } catch (
                        Exception e)

                {
                    e.printStackTrace();

                }

            }
        }).start();

    }


    private String parseSwitch(boolean on) {
        String cmd;
        if (on) {
            cmd = CMD_ON.replace("%id", String.valueOf(++mCmdId));
        } else {
            cmd = CMD_OFF.replace("%id", String.valueOf(++mCmdId));
        }
        return cmd;
    }


    private String parseToggle() {
        String cmd;

        cmd = CMD_TOGGLE.replace("%id", String.valueOf(++mCmdId));

        return cmd;
    }


    private String parseCTCmd(int ct) {
        return CMD_CT.replace("%id", String.valueOf(++mCmdId)).replace("%value", String.valueOf(ct ));
    }

    private String parseColorCmd(int color) {
        return CMD_HSV.replace("%id", String.valueOf(++mCmdId)).replace("%value", String.valueOf(color));
    }

    private String parseBrightnessCmd(int brightness) {
        return CMD_BRIGHTNESS.replace("%id", String.valueOf(++mCmdId)).replace("%value", String.valueOf(brightness));
    }

    private void write(final String cmd) {
        new Thread() {
            public void run() {
                if (mBos != null && mSocket.isConnected()) {
                    try {
                        mBos.write(cmd.getBytes());
                        mBos.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d(TAG, "mBos = null or mSocket is closed");
                }
            }
        }.start();

    }


    public void setBrightness(int brightness) {
        write(parseBrightnessCmd(brightness));
    }

    public void setCT(int CT) {
        write(parseCTCmd(CT));//+ 1700
    }

    public void setColor(int color) {
        write(parseColorCmd(color));
    }

    public void on() {
        write(parseSwitch(true));
    }

    public void off() {
        write(parseSwitch(false));
    }

    public void close() {
        try {
            cmd_run = false;
            if (mSocket != null)
                mSocket.close();

        } catch (Exception e) {

        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void getParams(String[] paramNames, final VolleyCallback callback) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", ++mCmdId);
            jsonObject.put("method", "get_prop");
            JSONArray params = new JSONArray(paramNames);
            jsonObject.put("params", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        write(jsonObject.toString());
    }

    public void toggle() {
        write(parseToggle());
    }
}
