package com.ho.smarthome;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.concurrent.atomic.AtomicInteger;

public class PaintView extends View {

    private static final int BACKGROUND = Color.TRANSPARENT;

    public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    private static String TAG = "PaintView";
    private AtomicInteger fingerX = new AtomicInteger(0);
    private AtomicInteger fingerY = new AtomicInteger(-100);



    boolean touching = false;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Log.d(TAG, "onDraw");

        int w = canvas.getWidth();
        int h = canvas.getHeight();

        canvas.drawColor(BACKGROUND);
        Paint paint = new Paint();

        canvas.drawColor(Color.WHITE);

        paint.setColor(Color.RED);

//        synchronized (this) {
        int cx = (fingerX.get() + 200) * w / 400;
        int cy = (-fingerY.get()) * h / 300;
        Log.d(TAG, "fingerX=" + fingerX.get() + " , fingerY=" + fingerY.get());
        Log.d(TAG, "cx=" + cx + " , cy=" + cy+" ,touching="+touching);
//        canvas.drawCircle(720, 170, 20, paint);

        int tempX=(0 + 200) * w / 400;
        int tempY=-(-100) * h / 300;
        Log.d(TAG, "tempX=" + tempX + " , tempY=" + tempY);
//        canvas.drawCircle(cx,cy , 20, paint);
        if (touching) {
            canvas.drawCircle(cx, cy, 20, paint);
        }


    }

    public void touch(int x, int y) {
        synchronized (this) {
            touching = true;
            this.fingerX.set(x);
            this.fingerY.set(y);
            Log.d(TAG, "touching=" + touching);
            Log.d(TAG, "fingerX=" + this.fingerX);
            Log.d(TAG, "fingerY=" + this.fingerY);
        }
//        postInvalidate();
//        invalidate();
    }

    public void endTouch() {

        touching = false;
//        postInvalidate();
//
//        invalidate();
    }



}