package com.ho.smarthome;

public interface VolleyCallback {
    void onSuccess(String result);
}