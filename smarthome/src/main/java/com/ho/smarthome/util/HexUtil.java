package com.ho.smarthome.util;

import java.math.BigInteger;

public class HexUtil {

	public static String asciiToHex(String ascii) {
		StringBuilder hex = new StringBuilder();

		for (int i = 0; i < ascii.length(); i++) {
			hex.append(Integer.toHexString(ascii.charAt(i)));
		}
		return hex.toString();
	}

	public static Integer hexToDec(String s) {
		int bytes = s.length();
		int result = 0;
		for (int i = 0; i < bytes; i++) {
			result += Character.getNumericValue(s.charAt(i)) * Math.pow(16, (bytes - i - 1));
		}
		return result;
	}

	public static String hexToAscii(String s) {
		int bytes = s.length();
		StringBuilder result = new StringBuilder();
		char temp;
		for (int i = 0; i < bytes; i += 2) {
			temp = (char) hexToDec(s.substring(i, i + 2)).intValue();
			result.append(temp);
		}
		return result.toString();
	}

	public static String bytesToHex(byte[] bytes) {
		StringBuilder builder = new StringBuilder();
		for (byte b: bytes) {
			builder.append(String.format("%02x", b));
		}
		return builder.toString();
	}

	public static byte[] hexStringToByte(String msgInHex){
		return new BigInteger(msgInHex, 16).toByteArray();
	}
}
