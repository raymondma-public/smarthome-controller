package com.ho.smarthome.welle;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;


public class WelleService extends Service {
    private Handler handler = new Handler();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.i("time:", "onStart");
        startRunner(showTime);
        super.onStart(intent, startId);

    }


    @Override
    public void onDestroy() {
        Log.i("time:", "onDestroy");
        stopRunner();
        super.onDestroy();
    }


    private void startRunner(Runnable showTime) {
        handler.postDelayed(showTime, 1000);
    }

    private void stopRunner() {
        handler.removeCallbacks(showTime);
    }

    private Runnable showTime = new Runnable() {
        public void run() {
            //log目前時間
//            Log.i("time:", new Date().toString());
            handler.postDelayed(this, 1000);
        }
    };


}
