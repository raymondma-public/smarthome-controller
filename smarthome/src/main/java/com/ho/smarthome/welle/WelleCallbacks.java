package com.ho.smarthome.welle;


public interface WelleCallbacks {
    void up();
    void down();
    void left();
    void right();
    void click();
    void clockwise();
}
