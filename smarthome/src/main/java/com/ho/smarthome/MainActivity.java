package com.ho.smarthome;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jean.jcplayer.JcAudio;
import com.example.jean.jcplayer.JcPlayerView;

import com.example.jean.jcplayer.JcStatus;
import com.ho.smarthome.ble.ClientManager;
import com.ho.smarthome.util.HexUtil;
import com.ho.smarthome.welle.WelleService;
import com.inuker.bluetooth.library.Code;
import com.inuker.bluetooth.library.connect.listener.BluetoothStateListener;
import com.inuker.bluetooth.library.connect.response.BleConnectResponse;
import com.inuker.bluetooth.library.connect.response.BleNotifyResponse;
import com.inuker.bluetooth.library.connect.response.BleReadResponse;
import com.inuker.bluetooth.library.connect.response.BleWriteResponse;
import com.inuker.bluetooth.library.model.BleGattCharacter;
import com.inuker.bluetooth.library.model.BleGattProfile;
import com.inuker.bluetooth.library.model.BleGattService;
import com.inuker.bluetooth.library.search.SearchRequest;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.inuker.bluetooth.library.utils.BluetoothLog;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


public class MainActivity extends AppCompatActivity
        implements JcPlayerView.OnInvalidPathListener, JcPlayerView.JcPlayerViewStatusListener {

    private static final String TAG = "Main";
//    private static final String TAG = MainActivity.class.getSimpleName();

    private JcPlayerView player;
    private RecyclerView recyclerView;
    private AudioAdapter audioAdapter;


    //Audio Manager
    private AudioManager audioManager;

    //Text to speech
    TextToSpeech tts;


    /*==========start light var================*/

    private ListView mListView;
    private MyAdapter mAdapter;
    List<HashMap<String, String>> mDeviceList = new ArrayList<HashMap<String, String>>();
    private TextView mTextView;
    private Button mBtnSearch;

    private YeelightFinder yeelightFinder;

    private YeelightCaller light;


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case YeelightFinder.MSG_FOUND_DEVICE:
                    mAdapter.notifyDataSetChanged();
                    connectLight();
                    break;
                case YeelightFinder.MSG_SHOWLOG:
                    Toast.makeText(MainActivity.this, "" + msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    break;
                case YeelightFinder.MSG_STOP_SEARCH:
                    mAdapter.notifyDataSetChanged();
                    yeelightFinder.stopSearching();
                    connectLight();
                    break;
                case YeelightFinder.MSG_DISCOVER_FINISH:
                    mAdapter.notifyDataSetChanged();
                    connectLight();
                    break;
            }
        }
    };

    private void connectLight() {
        if (mDeviceList.size() > 0) {
            String ipinfo = mDeviceList.get(0).get("Location").split("//")[1];
            String ip = ipinfo.split(":")[0];
            String port = ipinfo.split(":")[1];
            light = new YeelightCaller(ip, Integer.valueOf(port));
            light.connect();
            currentBrightness = Integer.valueOf(mDeviceList.get(0).get("bright").trim());
            currentCT = Integer.valueOf(mDeviceList.get(0).get("ct").trim());
            speak("connect light ");
        }
    }

    private WifiManager.MulticastLock multicastLock;

    /*===========end light var=================*/


    /*============start Welle var============*/


    private List<SearchResult> mDevices;
    private SearchResult welle;
    PaintView pv;
    /*============endWelle var===========*/

    //===================Recognize Gesture
    private static boolean isTounching = false;
    private static int oX = 0;
    private static int oY = 0;
    private static Date oDate = new Date();

    private static int lastX = 0;
    private static int lastY = 0;
    private static Date lastDate = new Date();

    private static final long timeForNextTouch = 100;
    private static final long timeForLongClick = 3000;
    private static final long clickDistanceError = 20;

    //===================

    private static final int PERMISSION_LOCATION_REQUEST_CODE = 1;

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ;
    }

    private void showPermissionDialog() {
        if (!checkPermission(this)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_LOCATION_REQUEST_CODE
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showPermissionDialog();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        player = (JcPlayerView) findViewById(R.id.jcplayer);

        ArrayList<JcAudio> jcAudios = new ArrayList<>();
        //jcAudios.add(JcAudio.createFromURL("url audio","http://www.villopim.com.br/android/Music_01.mp3"));
        jcAudios.add(JcAudio.createFromAssets("Asset audio 1", "49.v4.mid"));
        jcAudios.add(JcAudio.createFromAssets("Asset audio 2", "56.mid"));
        jcAudios.add(JcAudio.createFromAssets("Asset audio 3", "a_34.mp3"));
        jcAudios.add(JcAudio.createFromRaw("Raw audio 1", R.raw.a_34));
        jcAudios.add(JcAudio.createFromRaw("Raw audio 2", R.raw.a_203));
        //jcAudios.add(JcAudio.createFromFilePath("File directory audio", this.getFilesDir() + "/" + "CANTO DA GRAÚNA.mp3"));
        //jcAudios.add(JcAudio.createFromAssets("I am invalid audio", "aaa.mid")); // invalid assets file
        player.initPlaylist(jcAudios);

//        player.playAudio(player.getMyPlaylist().get(1));


//        jcAudios.add(JcAudio.createFromFilePath("test", this.getFilesDir() + "/" + "13.mid"));
//        jcAudios.add(JcAudio.createFromFilePath("test", this.getFilesDir() + "/" + "123123.mid")); // invalid file path
//        jcAudios.add(JcAudio.createFromAssets("49.v4.mid"));
//        jcAudios.add(JcAudio.createFromRaw(R.raw.a_203));
//        jcAudios.add(JcAudio.createFromRaw("a_34", R.raw.a_34));
//        player.initWithTitlePlaylist(jcAudios, "Awesome music");


//        jcAudios.add(JcAudio.createFromFilePath("test", this.getFilesDir() + "/" + "13.mid"));
//        jcAudios.add(JcAudio.createFromFilePath("test", this.getFilesDir() + "/" + "123123.mid")); // invalid file path
//        jcAudios.add(JcAudio.createFromAssets("49.v4.mid"));
//        jcAudios.add(JcAudio.createFromRaw(R.raw.a_203));
//        jcAudios.add(JcAudio.createFromRaw("a_34", R.raw.a_34));
//        player.initAnonPlaylist(jcAudios);

//        Adding new audios to playlist
//        player.addAudio(JcAudio.createFromURL("url audio","http://www.villopim.com.br/android/Music_01.mp3"));
//        player.addAudio(JcAudio.createFromAssets("49.v4.mid"));
//        player.addAudio(JcAudio.createFromRaw(R.raw.a_34));
//        player.addAudio(JcAudio.createFromFilePath(this.getFilesDir() + "/" + "121212.mmid"));

        player.registerInvalidPathListener(this);
        player.registerStatusListener(this);
        adapterSetup();

        //audio manager
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);


        //Text to speach
        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                }
            }
        });




        /*=========start light oncreate============*/
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        yeelightFinder = new YeelightFinder();

        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        multicastLock = wm.createMulticastLock("test");
        multicastLock.acquire();
        mTextView = (TextView) findViewById(R.id.infotext);
        mBtnSearch = (Button) findViewById(R.id.btn_search);
        mBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDeviceList.clear();
                mAdapter.notifyDataSetChanged();
                yeelightFinder.searchDevice(mHandler, mDeviceList);
            }

        });
        mListView = (ListView) findViewById(R.id.deviceList);
        mAdapter = new MyAdapter(this);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> bulbInfo = mDeviceList.get(position);
                Intent intent = new Intent(MainActivity.this, ControlActivity.class);
                String ipinfo = bulbInfo.get("Location").split("//")[1];
                String ip = ipinfo.split(":")[0];
                String port = ipinfo.split(":")[1];
                intent.putExtra("bulbinfo", bulbInfo);
                intent.putExtra("ip", ip);
                intent.putExtra("port", port);
                startActivity(intent);
            }
        });


        mDeviceList.clear();
        mAdapter.notifyDataSetChanged();
        yeelightFinder.searchDevice(mHandler, mDeviceList);

        /*==========end light oncreate=============*/



        /*==========start Welle oncreate================*/


        mDevices = new ArrayList();
        searchAndConnectWelle();
        ClientManager.getClient().registerBluetoothStateListener(new BluetoothStateListener() {
            @Override
            public void onBluetoothStateChanged(boolean openOrClosed) {
                BluetoothLog.v(String.format("onBluetoothStateChanged %b", openOrClosed));
            }
        });

        Intent intent = new Intent(MainActivity.this, WelleService.class);
        startService(intent);
        startRunner(endTouchChecker);
        pv = (PaintView) findViewById(R.id.paintView);

         /*==========end Welle oncreate================*/


    }

    protected void adapterSetup() {
        audioAdapter = new AudioAdapter(player.getMyPlaylist());
        audioAdapter.setOnItemClickListener(new AudioAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                player.playAudio(player.getMyPlaylist().get(position));
            }

            @Override
            public void onSongItemDeleteClicked(int position) {
                Toast.makeText(MainActivity.this, "Delete song at position " + position,
                        Toast.LENGTH_SHORT).show();
//                if(player.getCurrentPlayedAudio() != null) {
//                    Toast.makeText(MainActivity.this, "Current audio = " + player.getCurrentPlayedAudio().getPath(),
//                            Toast.LENGTH_SHORT).show();
//                }
                removeItem(position);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(audioAdapter);

        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

    }

    @Override
    public void onPause() {
        super.onPause();
        //speaker
        player.createNotification();
        //light
        yeelightFinder.endListenNotification();

        //welle
        ClientManager.getClient().stopSearch();
        stopRunner();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.kill();
        multicastLock.release();

        stopDataFlow();

        //welle
        Intent intent = new Intent(MainActivity.this, WelleService.class);
        stopService(intent);
        stopRunner();
    }


    public void onStart() {
        super.onStart();

    }

    @Override
    public void onPathError(JcAudio jcAudio) {
        Toast.makeText(this, jcAudio.getPath() + " with problems", Toast.LENGTH_LONG).show();
//        player.removeAudio(jcAudio);
//        player.next();
    }

    private void removeItem(int position) {
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(true);

        //        jcAudios.remove(position);
        player.removeAudio(player.getMyPlaylist().get(position));
        audioAdapter.notifyItemRemoved(position);

        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    @Override
    public void onPausedStatus(JcStatus jcStatus) {

    }

    @Override
    public void onContinueAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPlayingStatus(JcStatus jcStatus) {

    }

    @Override
    public void onTimeChangedStatus(JcStatus jcStatus) {
        updateProgress(jcStatus);
    }

    @Override
    public void onCompletedAudioStatus(JcStatus jcStatus) {
        updateProgress(jcStatus);
    }

    @Override
    public void onPreparedAudioStatus(JcStatus jcStatus) {
    }

    private void updateProgress(final JcStatus jcStatus) {
        Log.d(TAG, "Song id = " + jcStatus.getJcAudio().getId() + ", song duration = " + jcStatus.getDuration()
                + "\n song position = " + jcStatus.getCurrentPosition());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // calculate progress
                float progress = (float) (jcStatus.getDuration() - jcStatus.getCurrentPosition())
                        / (float) jcStatus.getDuration();
                progress = 1.0f - progress;
                audioAdapter.updateProgress(jcStatus.getJcAudio(), progress);
            }
        });
    }

    ////////////////////////////////////////////////////////////////

    public void playNext() {
        boolean wasPlaying = player.isPlaying();

        String actionMessage = "next Music";
        speak(actionMessage);
        player.next();
        if (!wasPlaying) {
            player.pause();
        }
    }

    private void speak(String messageToSpeak) {
        tts.speak(messageToSpeak, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void playPrevious() {
        boolean wasPlaying = player.isPlaying();

        String actionMessage = "previous Music";
        speak(actionMessage);

        player.previous();
        if (!wasPlaying) {
            player.pause();
        }
    }

    public void togglePlayMusic() {
        if (player.isPaused()) {
            String actionMessage = "play music";
            speak(actionMessage);
            player.continueAudio();
        } else {
            String actionMessage = "pause music";
            speak(actionMessage);
            player.pause();
        }

    }

    public void raiseVolume() {

        audioManager.adjustVolume(AudioManager.ADJUST_RAISE, 0);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        speak("" + currentVolume);
    }


    public void lowerVolume() {

        audioManager.adjustVolume(AudioManager.ADJUST_LOWER, 0);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        speak("" + currentVolume);
    }


    private static enum ControlMode {
        MENU,
        Light,
        Speaker
    }

    ;
    private ControlMode currentMode = ControlMode.MENU;
    private int menuIdxPtr = 0;
    private ControlMode[] menu = new ControlMode[]{ControlMode.Light, ControlMode.Speaker};


    int currentCT = 0;
    int currentBrightness = 0;

    private static final int ctStep = 800;
    private static final int brightnessStep = 10;

    public void upBtnClick(View view) {
        moveUp();
    }


    public void downBtnClick(View view) {
        moveDown();
    }


    public void leftBtnClick(View view) {
        moveLeft();
    }


    public void rightBtnClick(View view) {
        moveRight();
    }


    public void middleBtnClick(View view) {

        shortClick();
    }


    private void moveUp() {
        Log.d(TAG, "Move up");
        switch (currentMode) {
            case MENU:
                menuUp();
                break;
            case Light:


                if (light != null) {
                    speak("increase brightness");
                    if (currentBrightness + brightnessStep <= MAX_BRIGHTNESS) {
                        light.setBrightness(currentBrightness + brightnessStep);
                        currentBrightness += brightnessStep;
                    } else {
                        light.setBrightness(MAX_BRIGHTNESS);
                        currentBrightness = MAX_BRIGHTNESS;
                        speak("reach maximum brightness");
                    }
                } else {
                    speak("light is not connected");
                    connectLight();
                }

                break;
            case Speaker:
                raiseVolume();
                break;
            default:
                break;
        }

    }

    private void menuUp() {
        menuIdxPtr++;
        menuIdxPtr %= menu.length;
        speakMenuPointing();
    }

    private static int MIN_BRIGHTNESS = 1;
    private static int MAX_BRIGHTNESS = 100;

    private static int MIN_COLOR = 0;
    private static int MAX_COLOR = 350;

    private static int MIN_CT = 1700;
    private static int MAX_CT = 6500;

    private void moveDown() {
        Log.d(TAG, "Move down");
        switch (currentMode) {
            case MENU:
                menuDown();
                break;
            case Light:

                if (light != null) {
                    speak("decrease brightness");
                    if (currentBrightness - brightnessStep >= MIN_BRIGHTNESS) {
                        light.setBrightness(currentBrightness - brightnessStep);
                        currentBrightness -= brightnessStep;
                    } else {
                        light.setBrightness(MIN_BRIGHTNESS);
                        currentBrightness = MIN_BRIGHTNESS;
                        speak("reach minimum brightness");
                    }
                    break;
                } else {
                    speak("light is not connected");
                    connectLight();
                }
            case Speaker:
                lowerVolume();
                break;
            default:
                break;
        }

    }

    private void moveLeft() {
        Log.d(TAG, "Move Left");
        switch (currentMode) {
            case MENU:
                menuDown();
                break;
            case Light:
                if (light != null) {
                    speak("decrease color temperature");
                    if (currentCT - ctStep >= MIN_CT) {
                        light.setCT(currentCT - ctStep);
                        currentCT -= ctStep;
                    } else {
                        light.setBrightness(MIN_CT);
                        currentBrightness = MIN_CT;
                        speak("reach minimum color temperature");
                    }

                    Log.d(TAG, "currentCT=" + currentCT);

                } else {

                    speak("light is not connected");

                }

                break;
            case Speaker:
                playPrevious();
                break;
            default:
                break;
        }

    }

    private void menuDown() {
        menuIdxPtr--;
        if (menuIdxPtr < 0) {
            menuIdxPtr += menu.length;
        }

        speakMenuPointing();
    }

    private void moveRight() {
        Log.d(TAG, "Move Right");
        switch (currentMode) {
            case MENU:
                menuUp();
                break;
            case Light:
                if (light != null) {
                    speak("increase color temperature");
                    if (currentCT + ctStep <= MAX_CT) {
                        light.setCT(currentCT + ctStep);
                        currentCT += ctStep;
                    } else {
                        light.setBrightness(MAX_CT);
                        currentBrightness = MAX_CT;
                        speak("reach maximum color temperature");
                    }
                    Log.d(TAG, "currentCT=" + currentCT);
                } else {
                    speak("light is not connected");
                    connectLight();
                }
                break;
            case Speaker:
                playNext();
                break;
            default:
                break;
        }

    }

    private void shortClick() {
        Log.d(TAG, "Short Click");
        switch (currentMode) {
            case MENU:
                speak("Go to control " + menu[menuIdxPtr]);
                currentMode = menu[menuIdxPtr];
                break;
            case Light:

                if (light != null) {
                    speak("toggle light");
                    light.toggle();
                } else {
                    speak("light is not connected");
                    connectLight();
                }
                break;
            case Speaker:
                togglePlayMusic();
                break;
            default:
                break;
        }

    }

    private void longClick() {
        Log.d(TAG, "Long Click");
        if (currentMode == ControlMode.MENU) {
            speakMenuPointing();
        } else {
            speak("Long Click, Back to Menu");
            currentMode = ControlMode.MENU;
        }
    }

    private void speakMenuPointing() {
        speak("You are now in Menu, pointing to " + menu[menuIdxPtr].name());
    }

    /*===========start light methods===============*/


    @Override
    protected void onResume() {
        super.onResume();
        yeelightFinder.keepListenNotification(mHandler, mDeviceList);

        startRunner(endTouchChecker);


    }


    public void recalWelle(View view) {
        reCalWelle();
    }

    public void startDF(View view) {
        startDataFlow();
    }

    public void stopDF(View view) {
        stopDataFlow();
    }

    public void getBattery(View view) {
        getBattery();
    }

    private class MyAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        private int mLayoutResource;

        public MyAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
            mLayoutResource = android.R.layout.simple_list_item_2;
        }

        @Override
        public int getCount() {
            return mDeviceList.size();
        }

        @Override
        public Object getItem(int position) {
            return mDeviceList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            HashMap<String, String> data = (HashMap<String, String>) getItem(position);
            if (convertView == null) {
                view = mLayoutInflater.inflate(mLayoutResource, parent, false);
            } else {
                view = convertView;
            }
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText("Type = " + data.get("model"));

            Log.d(TAG, "name = " + textView.getText().toString());
            TextView textSub = (TextView) view.findViewById(android.R.id.text2);
            textSub.setText("location = " + data.get("Location"));
            return view;
        }
    }



    /*============end light methods===============*/


    /*============start welle methods===============*/
    private void searchAndConnectWelle() {
        SearchRequest request = new SearchRequest.Builder()
                .searchBluetoothLeDevice(5000, 2).build();

        ClientManager.getClient().search(request, mSearchResponse);
    }


    private final SearchResponse mSearchResponse = new SearchResponse() {
        @Override
        public void onSearchStarted() {
            BluetoothLog.w("MainActivity.onSearchStarted");
        }

        @Override
        public void onDeviceFounded(SearchResult device) {
            BluetoothLog.w("MainActivity.onDeviceFounded " + device.device.getAddress());
            if (!mDevices.contains(device)) {
                mDevices.add(device);

            }

            if (mDevices.size() > 0) {
                String devicesString = "";
//                for (int i = 0; i < mDevices.size(); i++) {
//                    SearchResult currentDevice = mDevices.get(i);
//                    String mac = currentDevice.getAddress();
//                    String name = currentDevice.getName();
//                    devicesString += i + ":  name=" + name + " , mac=" + mac + "\n";
//                }
                BluetoothLog.w("devicesString:\n" + devicesString);
                Log.d("BluetoothLog", devicesString);

            }
            if (device.getName().equals("Welle")) {
                if (welle == null) {
                    ClientManager.getClient().stopSearch();
                    welle = device;


                    ClientManager.getClient().connect(welle.getAddress(), new BleConnectResponse() {
                        @Override
                        public void onResponse(int code, BleGattProfile profile) {
                            if (code == Code.REQUEST_SUCCESS) {
                                Toast.makeText(getApplicationContext(), "Welle Connected", Toast.LENGTH_SHORT).show();
                                speak("Welle connected");
                                BluetoothLog.v(String.format("profile:\n%s", profile));
                                for (BleGattService service : profile.getServices()) {

                                    String serviceUUID = service.getUUID().toString();
                                    for (BleGattCharacter character : service.getCharacters()) {
                                        String charUuid = character.getUuid().toString();
                                        Log.d("Welle Info", "serviceUUID: " + serviceUUID + " , charUuid=" + charUuid);
                                    }
                                }


                                listenNotify();
                                startDataFlow();

                            }
                        }
                    });
                }


            }

        }


        @Override
        public void onSearchStopped() {
            BluetoothLog.w("MainActivity.onSearchStopped");
//            mListView.onRefreshComplete(true);
//            mRefreshLayout.showState(AppConstants.LIST);
//
//            mTvTitle.setText(R.string.devices);
        }

        @Override
        public void onSearchCanceled() {
            BluetoothLog.w("MainActivity.onSearchCanceled");

//            mListView.onRefreshComplete(true);
//            mRefreshLayout.showState(AppConstants.LIST);
//
//            mTvTitle.setText(R.string.devices);
        }
    };


    private static final String READ_SERVICE_UUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
    private static final String READ_CHAR_UUID = "0000ffe4-0000-1000-8000-00805f9b34fb";

    private static final String WRITE_SERVICE_UUID = "0000ffe5-0000-1000-8000-00805f9b34fb";
    private static final String WRITE_CHAR_UUID = "0000ffe9-0000-1000-8000-00805f9b34fb";

    private void startDataFlow() {
        UUID serviceUUID = UUID.fromString(WRITE_SERVICE_UUID);
        UUID characterUUID = UUID.fromString(WRITE_CHAR_UUID);

        String hexMsg = "232323232323000630011000";
        byte[] bytesMsg = HexUtil.hexStringToByte(hexMsg);


        if (welle != null) {
            ClientManager.getClient().write(welle.getAddress(), serviceUUID, characterUUID, bytesMsg, new BleWriteResponse() {
                @Override
                public void onResponse(int code) {
                    if (code == Code.REQUEST_SUCCESS) {
                        Log.d(TAG, "start dataflow success");
                    } else {
                        Log.d(TAG, "start dataflow fail");
                    }
                }
            });
        } else {
            Log.d(TAG, "no welle");
        }
    }


    private void stopDataFlow() {
        UUID serviceUUID = UUID.fromString(WRITE_SERVICE_UUID);
        UUID characterUUID = UUID.fromString(WRITE_CHAR_UUID);
        byte[] bytesMsg = HexUtil.hexStringToByte("232323232323000630012000");
        if (welle != null) {
            ClientManager.getClient().write(welle.getAddress(), serviceUUID, characterUUID, bytesMsg, new BleWriteResponse() {
                @Override
                public void onResponse(int code) {
                    if (code == Code.REQUEST_SUCCESS) {
                        Log.d(TAG, "stop  dataflow success");
                    } else {
                        Log.d(TAG, "stop  dataflow fail");
                    }
                }
            });
        } else {
            Log.d(TAG, "no welle");
            searchAndConnectWelle();
        }
    }


    private void reCalWelle() {
        UUID serviceUUID = UUID.fromString(WRITE_SERVICE_UUID);
        UUID characterUUID = UUID.fromString(WRITE_CHAR_UUID);
        byte[] bytesMsg = HexUtil.hexStringToByte("232323232323000640010008");
        if (welle != null) {
            ClientManager.getClient().write(welle.getAddress(), serviceUUID, characterUUID, bytesMsg, new BleWriteResponse() {
                @Override
                public void onResponse(int code) {
                    if (code == Code.REQUEST_SUCCESS) {
                        Log.d(TAG, "reCalWelle success");
                    } else {
                        Log.d(TAG, "reCalWelle fail");
                    }
                }
            });
        } else {
            Log.d(TAG, "no welle");
            searchAndConnectWelle();
        }
    }

    private void getBattery() {
        UUID serviceUUID = UUID.fromString(WRITE_SERVICE_UUID);
        UUID characterUUID = UUID.fromString(WRITE_CHAR_UUID);
        byte[] bytesMsg = HexUtil.hexStringToByte("232323232323000610010203");
        if (welle != null) {
            ClientManager.getClient().write(welle.getAddress(), serviceUUID, characterUUID, bytesMsg, new BleWriteResponse() {
                @Override
                public void onResponse(int code) {
                    if (code == Code.REQUEST_SUCCESS) {
                        Log.d(TAG, "getBattery success");
                        readWelle();

                    } else {
                        Log.d(TAG, "getBattery fail");
                    }
                }
            });
        } else {
            Log.d(TAG, "no welle");
            searchAndConnectWelle();
        }
    }


    private void readWelle() {
        UUID serviceUUID = UUID.fromString(READ_SERVICE_UUID);
        UUID characterUUID = UUID.fromString(READ_CHAR_UUID);
        if (welle != null) {
            ClientManager.getClient().read(welle.getAddress(), serviceUUID, characterUUID, new BleReadResponse() {
//                @Override
//                public void onNotify(UUID service, UUID character, byte[] data) {
//                    Log.d("Welle Data:", HexUtil.bytesToHex(data));
//                    String dataHexString=HexUtil.bytesToHex(data);
//                    if(dataHexString.startsWith("232323232323")){
//                        //Welle data
////                        Toast.makeText(MainActivity.this, dataHexString , Toast.LENGTH_SHORT).show();
//
//                        if(dataHexString.substring(16, 16+4).equals("5001")){
//                            Toast.makeText(MainActivity.this, "wNotification" , Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }

                @Override
                public void onResponse(int code, byte[] data) {
                    Log.d(TAG, "code=" + code);

                    if (code == Code.REQUEST_SUCCESS) {
                        Log.d(TAG, "read  success");
                        if (data != null) {
                            Log.d("Welle Data:", HexUtil.bytesToHex(data));
                        }
                    } else {
                        Log.d(TAG, "read  fail");
                    }
                }
            });
        } else {
            Log.d(TAG, "no welle");
            searchAndConnectWelle();
        }


    }

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    int[] position = (int[]) msg.obj;
                    int x = position[0];
                    int y = position[1];
                    pv.touch(x, y);
                    pv.postInvalidate();
                    break;
                case 2:
                    pv.endTouch();
                    pv.postInvalidate();

            }
            super.handleMessage(msg);
        }
    };

    private void listenNotify() {
        UUID serviceUUID = UUID.fromString(READ_SERVICE_UUID);
        UUID characterUUID = UUID.fromString(READ_CHAR_UUID);
        if (welle != null) {
            ClientManager.getClient().notify(welle.getAddress(), serviceUUID, characterUUID, new BleNotifyResponse() {
                @Override
                public void onNotify(UUID service, UUID character, byte[] data) {
                    Log.d("Welle Data:", HexUtil.bytesToHex(data));

                    String dataHexString = HexUtil.bytesToHex(data);
                    Log.d("Welle Data:", "first 4=" + dataHexString.substring(0, 0 + 4));
                    if (dataHexString.substring(0, 0 + 4).equals("2123")) {
                        String xString = dataHexString.substring(4, 4 + 4);
                        String yString = dataHexString.substring(10, 10 + 4);
                        int x = HexUtil.hexToDec(xString);
                        if (x >= 32767) {
                            x = (65536 - x) * (-1);
                        }
                        int y = HexUtil.hexToDec(yString);
                        if (y >= 32767) {
                            y = (65536 - y) * (-1);
                        }
                        Log.d("coordinate", "x=" + x + " ,y=" + y);

                        pv.touch(x, y);
                        Message message = new Message();
                        message.what = 1;
                        message.obj = new int[]{x, y};
                        MainActivity.this.uiHandler.sendMessage(message);

                        int currentX = x;
                        int currentY = y;
                        Date currentDate = new Date();

                        long diffInMillies = currentDate.getTime() - lastDate.getTime();
                        Log.d(TAG, "diffInMillies=" + diffInMillies);
                        if (diffInMillies >= timeForNextTouch) {
                            Log.d(TAG, "Start Touch");
                            isTounching = true;
                            oX = currentX;
                            oY = currentY;
                            oDate = currentDate;
                        }
                        lastX = currentX;
                        lastY = currentY;
                        lastDate = currentDate;


//                            Toast.makeText(MainActivity.this, "wNotification", Toast.LENGTH_SHORT).show();
                    }


                    if (dataHexString.startsWith("232323232323")) {
                        //Welle data
//                        Toast.makeText(MainActivity.this, dataHexString , Toast.LENGTH_SHORT).show();


                        if (dataHexString.substring(16, 16 + 4).equals("5001")) {
                            Toast.makeText(MainActivity.this, "wNotification", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onResponse(int code) {
                    Log.d(TAG, "code=" + code);
                    if (code == Code.REQUEST_SUCCESS) {
                        Log.d(TAG, "read  success");
                    }

                }
            });
        } else {
            Log.d(TAG, "no welle");
            searchAndConnectWelle();
        }


    }


    private Handler handler = new Handler();

    public void startRunner(Runnable showTime) {
        handler.postDelayed(showTime, 50);
    }

    public void stopRunner() {
        handler.removeCallbacks(endTouchChecker);
    }

    private Runnable endTouchChecker = new Runnable() {
        public void run() {
            //log目前時間
//            Log.i("time:", new Date().toString());
            Date currentDate = new Date();
            long diffInMillies = currentDate.getTime() - lastDate.getTime();
//            Log.d(TAG, "diffInMillies=" + diffInMillies + " , isTounching=" + isTounching);
            if (diffInMillies >= timeForNextTouch && isTounching) {

                Log.d(TAG, "End Touch");
                pv.endTouch();
                Message message = new Message();
                message.what = 2;
                MainActivity.this.uiHandler.sendMessage(message);

                double distance = Math.hypot(oX - lastX, oY - lastY);
                isTounching = false;
                if (distance <= clickDistanceError) {

                    long clickDiffInMillies = currentDate.getTime() - oDate.getTime();
                    if (clickDiffInMillies >= timeForLongClick) {
                        Log.d(TAG, "This is a Long Click");
                        longClick();
                    } else {
                        Log.d(TAG, "This is a Short Click");
                        shortClick();
                    }
                    handler.postDelayed(this, 50);
                    return;
                }

                int xMag = Math.abs(oX - lastX);
                int yMag = Math.abs(oY - lastY);


                if (xMag > yMag) {//move horizontally
                    if (lastX - oX <= 0) {//moved left
                        Log.d(TAG, "Moved Left");
                        moveLeft();
                    } else {//moved right
                        Log.d(TAG, "Moved Right");
                        moveRight();
                    }

                } else {//move vertically

                    if (lastY - oY >= 0) {//moved up
                        Log.d(TAG, "Moved Up");
                        moveUp();
                    } else {//moved down
                        Log.d(TAG, "Moved Down");
                        moveDown();
                    }
                }

            }

            handler.postDelayed(this, 50);
        }
    };

     /*============end welle methods===============*/
}
